import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	phone: DS.attr('number'),
	date: DS.attr('number'),
	credit: DS.attr('number'),
	product: DS.hasMany('product')
});
