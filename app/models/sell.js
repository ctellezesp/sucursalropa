import DS from 'ember-data';

export default DS.Model.extend({
	date: DS.attr('number'),
	total: DS.attr('number'),
	products: DS.hasMany('product')
});
